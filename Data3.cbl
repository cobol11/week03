       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA3.
       AUTHOR. THANYARAK.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 SURNAME PIC X(8) VALUE "COUGHLAN".
       01 SALE-PRICE PIC 9(4)V99.
       01 NUM-OF-EMPLOYEES PIC 999V99.
       01 SALARY PIC 9(4)V99.
       01 COUNTY-NAME PIC X(9).
       PROCEDURE DIVISION.
       ShowDisplay.
           DISPLAY "1 " SURNAME
           MOVE "SMITH" TO SURNAME
           DISPLAY "2 " SURNAME
           MOVE "FITZWILLIAM" TO SURNAME
           DISPLAY "3 " SURNAME
           DISPLAY "----------------------"
           .
           DISPLAY "1 " SALE-PRICE
           MOVE ZEROS TO SALE-PRICE
           DISPLAY "2 " SALE-PRICE
      *     01 SALE-PRICE PIC 9(4)V99 0025.50
           MOVE 25.5 TO SALE-PRICE
           DISPLAY "3 " SALE-PRICE
      *     01 SALE-PRICE PIC 9(4)V99 0007.55
           MOVE 7.553 TO SALE-PRICE
           DISPLAY "4 " SALE-PRICE      
      *     01 SALE-PRICE PIC 9(4)V99 3425.21
           MOVE 93425.215 TO SALE-PRICE
           DISPLAY "5 " SALE-PRICE
      *     01 SALE-PRICE PIC 9(4)V99 0127.00
           MOVE 127 TO SALE-PRICE
           DISPLAY "6 " SALE-PRICE
           DISPLAY "----------------------"
           .
      *    01 SALARY PIC 999V99.
           DISPLAY NUM-OF-EMPLOYEES
      *    01 SALARY PIC 999V99.  012.40
           MOVE 12.4 TO NUM-OF-EMPLOYEES
           DISPLAY NUM-OF-EMPLOYEES
      *    01 SALARY PIC 999V99.  745.00
           MOVE 6745 TO NUM-OF-EMPLOYEES
           DISPLAY NUM-OF-EMPLOYEES
      *    01 SALARY PIC 9(4)V99.  0745.00
           MOVE NUM-OF-EMPLOYEES TO SALARY
           DISPLAY SALARY
           DISPLAY "----------------------"
           .
           MOVE "GALWAY" TO COUNTY-NAME
           DISPLAY COUNTY-NAME
           MOVE ALL "CREAM_" TO COUNTY-NAME
           DISPLAY COUNTY-NAME
           .